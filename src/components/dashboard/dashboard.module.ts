import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from 'src/shared/shared.module';
import { NgxWidgetGridModule } from 'ngx-widget-grid';
import { MapModule } from '../map/map.module';
import { DebtorListModule } from '../debtor-list/debtor-list.module';
import { KillerListModule } from '../killer-list/killer-list.module';
import { NavModule } from '../nav/nav.module';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    SharedModule,
    NgxWidgetGridModule,
    MapModule,
    DebtorListModule,
    KillerListModule,
    NavModule
  ]
})
export class DashboardModule { }
