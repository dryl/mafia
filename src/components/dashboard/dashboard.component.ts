import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Debtor } from 'src/models/Debtor';
import { Killer } from 'src/models/Killer';
import { Widget } from 'src/models/Widget';
import { KillersService } from 'src/services/killers.service';
import { DebtorsService } from 'src/services/debtors.service';
import { DashboardService } from 'src/services/dashboard.service';
import { first } from 'rxjs/operators';
import { Location } from '../../models/Location';
import { MapComponent } from '../map/map.component';
import { DebtorListComponent } from '../debtor-list/debtor-list.component';
import { KillerListComponent } from '../killer-list/killer-list.component';
import { Rectangle } from 'ngx-widget-grid';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  @ViewChild(MapComponent, { static: false }) map;
  @ViewChild(DebtorListComponent, { static: false }) debtorList;
  @ViewChild(KillerListComponent, { static: false }) killerList;

  editMode = false;
  mapActive = true;
  killerWidgetActive = true;
  debtorWidgetActive = true;

  widgetsCount = 3;
  debtors: Debtor[];
  killers: Killer[];
  widgets: Widget[];

  debtorsLocation: Location[];
  killersLocation: Location[];

  widgetListMenu: Widget[] = [
    { position: new Rectangle(), type: 0, options: { title: "Dłużnicy" }, },
    { position: new Rectangle(), type: 1, options: { title: "Zabójcy" }, },
    { position: new Rectangle(), type: 2, options: { title: "Mapa" }, }
  ];
  selectedWidget: number;

  tempWidgets: Widget[];
  tempWidgetListMenu: Widget[];

  constructor(private debtorsService: DebtorsService, private killersService: KillersService, private dashboardService: DashboardService, private matSnackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadDashboard();
    this.loadDebtors();
    this.loadKillers();
  }

  loadDashboard() {
    this.mapActive = false;
    this.debtorWidgetActive = false;
    this.killerWidgetActive = false;
    this.dashboardService.getDashboard().pipe(first()).subscribe(value => {
      this.widgets = [];
      this.tempWidgets = [];
      JSON.parse(value.widgets).forEach(item => {
        switch (item.type) {
          case 0:
            this.debtorWidgetActive = true;
            break;
          case 1:
            this.killerWidgetActive = true;
            break;
          case 2:
            this.mapActive = true;
            break;
        }
        this.widgets.push({ options: item.options, type: item.type, position: new Rectangle(item.position) })
      });
      this.widgets.forEach(item => {
        this.tempWidgets.push({ type: item.type, options: item.options, position: item.position });
      });
      if (this.widgets.length < this.widgetsCount) {
        this.widgets.map(item => item.type).filter(
          function (item) {
            return this.indexOf(item) < 0;
          }, this.widgetListMenu.map(item => item.type));

        let types = this.widgets.map(item => item.type);
        this.widgetListMenu = this.widgetListMenu.filter(item => {
          return !types.includes(item.type);
        });
      }
      else {
        this.widgetListMenu = [];
      }
      this.tempWidgetListMenu = this.widgetListMenu;
    },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
  }

  saveDashboard() {
    this.dashboardService.save(this.widgets).subscribe(result => {
      this.tempWidgets = this.widgets;
      this.tempWidgetListMenu = this.widgetListMenu;
    },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
  }

  getDefaultDashboard() {
    this.mapActive = false;
    this.killerWidgetActive = false;
    this.debtorWidgetActive = false;
    this.dashboardService.getDefaultDashboard().subscribe(result => {
      this.widgets = [];
      JSON.parse(result.widgets).forEach(item => {
        switch (item.type) {
          case 0:
            this.debtorWidgetActive = true;
            break;
          case 1:
            this.killerWidgetActive = true;
            break;
          case 2:
            this.mapActive = true;
            break;
        }
        this.widgets.push({ options: item.options, type: item.type, position: new Rectangle(item.position) })
      });
      if (this.widgets.length < this.widgetsCount) {
        this.widgets.map(item => item.type).filter(
          function (item) {
            return this.indexOf(item) < 0;
          }, this.widgetListMenu.map(item => item.type));

        let types = this.widgets.map(item => item.type);
        this.widgetListMenu = this.widgetListMenu.filter(item => {
          return !types.includes(item.type);
        });
      }
      else {
        this.widgetListMenu = [];
      }
    },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
  }

  discardChanges() {
    this.mapActive = false;
    this.killerWidgetActive = false;
    this.debtorWidgetActive = false;
    this.widgets = [];
    this.tempWidgets.forEach(item => {
      switch (item.type) {
        case 0:
          this.debtorWidgetActive = true;
          break;
        case 1:
          this.killerWidgetActive = true;
          break;
        case 2:
          this.mapActive = true;
          break;
      }
      this.widgets.push({ type: item.type, options: item.options, position: item.position });
    });
    this.widgetListMenu = [];
    this.tempWidgetListMenu.forEach(item => {
      this.widgetListMenu.push({ type: item.type, options: item.options, position: item.position });
    });
    this.widgetListMenu.forEach(item => {
      this.widgetListMenu.splice(this.widgetListMenu.indexOf(item), 1);
    });
  }

  loadDebtors() {
    this.debtorsService.getList().pipe(first()).subscribe(debtors => {
      this.debtors = debtors;
      this.debtorsLocation = this.debtors.map(item => this.parseLocation(item.location));
    },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
  }

  loadKillers() {
    this.killersService.getList().pipe(first()).subscribe(killers => {
      this.killers = killers;
      this.killersLocation = this.killers.map(item => this.parseLocation(item.location));
    },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
  }

  parseLocation(location: string) {
    let parsedLocation = new Location();
    parsedLocation.latitude = Number.parseFloat(location.split('/')[0]);
    parsedLocation.longitude = Number.parseFloat(location.split('/')[1]);
    return parsedLocation;
  }

  findOnMap(coordinates) {
    let location = Location.parseLocation(coordinates);
    this.map.zoomToPosition(location.latitude, location.longitude);
  }

  refreshDebtorList() {
    if (this.debtorWidgetActive) {
      this.debtorList.loadDebtors();
    }
  }

  refreshKillerList() {
    if (this.killerWidgetActive) {
      this.killerList.loadKillers();
    }
  }

  refreshLocations() {
    this.refreshDebtorsLocation();
    this.refreshKillerLocation();
  }

  refreshDebtorsLocation() {
    if (this.debtorList && this.debtorList.debtorList) {
      this.debtorsLocation = this.debtorList.debtorList.map(item => this.parseLocation(item.location));
    }
  }

  refreshKillerLocation() {
    if (this.killerList && this.killerList.killerList) {
      this.killersLocation = this.killerList.killerList.map(item => this.parseLocation(item.location));
    }
  }

  addWidget() {
    let widget = this.widgetListMenu.find(item => item.type == this.selectedWidget);
    switch (widget.type) {
      case 0:
        this.debtorWidgetActive = true;
        break;
      case 1:
        this.killerWidgetActive = true;
        break;
      case 2:
        this.mapActive = true;
        break;
    }
    this.widgetListMenu.splice(this.widgetListMenu.indexOf(widget), 1);
    this.widgets.push(widget);
    this.selectedWidget = null;
  }

  deleteWidget(widget: Widget) {
    switch (widget.type) {
      case 0:
        this.debtorWidgetActive = false;
        break;
      case 1:
        this.killerWidgetActive = false;
        break;
      case 2:
        this.mapActive = false;
        break;
    }
    this.widgets = this.widgets.filter(item => {
      if (item.type == widget.type) {
        this.widgetListMenu.push(widget);
      }
      return item.type != widget.type;
    });
  }
}