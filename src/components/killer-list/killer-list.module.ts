import { NgModule } from '@angular/core';
import { SharedModule } from 'src/shared/shared.module';
import { KillerListComponent } from './killer-list.component';
import { KillerDialogModule } from '../killer-dialog/killer-dialog.module';
import { KillerDialogComponent } from '../killer-dialog/killer-dialog.component';
import { AssignmentDialogComponent } from '../assignment-dialog/assignment-dialog.component';
import { AssignmentDialogModule } from '../assignment-dialog/assignment-dialog.module';



@NgModule({
  declarations: [KillerListComponent],
  imports: [
    SharedModule,
    KillerDialogModule,
    AssignmentDialogModule
  ],
  exports: [KillerListComponent],
  entryComponents: [
    KillerDialogComponent,
    AssignmentDialogComponent
  ]
})
export class KillerListModule { }
