import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { Killer } from 'src/models/Killer';
import { KillersService } from 'src/services/killers.service';
import { KillerDialogComponent } from '../killer-dialog/killer-dialog.component';
import { MatDialog, MatSort, MatSnackBar } from '@angular/material';
import { AssignmentDialogComponent } from '../assignment-dialog/assignment-dialog.component';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-killer-list',
  templateUrl: './killer-list.component.html',
  styleUrls: ['./killer-list.component.css']
})
export class KillerListComponent implements OnInit {

  killerList: Killer[];
  displayedColumns: string[];

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  dataSource: MatTableDataSource<Killer>;

  @Output() findOnMap = new EventEmitter<string>();
  @Output() refreshKillerList = new EventEmitter<void>();
  @Output() refreshDebtorList = new EventEmitter<void>();
  @Output() refreshLocations = new EventEmitter<void>();
  @Input() mapActive: boolean;
  @Input() debtorWidgetActive: boolean;
  @Input() killerWidgetActive: boolean;

  constructor(private killersService: KillersService, public matDialog: MatDialog, private matSnackBar: MatSnackBar) { }

  ngOnInit() {
    this.displayedColumns = ['pseudonym', 'salary', 'actions'];
    this.loadKillers();
  }

  initSort() {
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'pseudonym': return item[property].toLowerCase();
        default: return item[property];
      }
    };
  }

  loadKillers() {
    this.killersService.getList().subscribe(result => {
      this.killerList = result;
      this.refreshLocations.emit();
      this.dataSource = new MatTableDataSource(this.killerList);
      this.initSort();
    },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
  }

  openKillerDialog(killerId: number) {
    var element = document.getElementById("map");
    var node = document.getElementById("map").parentNode;
    node.removeChild(element);

    if (killerId) {
      var killer: Killer = this.killerList.find(item => item.id == killerId);
    }
    else {
      var killer = new Killer();
    }
    const dialog = this.matDialog.open(KillerDialogComponent, {
      width: '600px',
      data: {
        id: killer.id,
        pseudonym: killer.pseudonym,
        salary: killer.salary,
        location: killer.location,
        targetId: killer.targetId
      }
    });
    dialog.afterClosed().subscribe(result => {
      node.appendChild(element);
      this.loadKillers();
    });
  }

  openAssignmentDialog(killerId: number) {
    if (killerId) {
      var killer: Killer = this.killerList.find(item => item.id == killerId);
    }
    else {
      var killer = new Killer();
    }
    const dialog = this.matDialog.open(AssignmentDialogComponent, {
      width: '600px',
      data: {
        id: killer.id,
        pseudonym: killer.pseudonym,
        location: killer.location,
        targetId: killer.targetId
      }
    });
    dialog.afterClosed().subscribe(result => {
      this.refreshDebtorList.emit();
      if (result) {
        this.killerList[this.killerList.findIndex(item => item.id == result.id)] = result;
      }
    });
  }

  findOnMapClick(location: string) {
    this.findOnMap.emit(location);
  }

  deleteKiller(killer: Killer) {
    if (confirm("Czy na pewno chcesz usunąć wybranego zabójcę (" + killer.pseudonym + ")?")) {
      this.killersService.delete(killer.id).subscribe(result => {
        this.loadKillers();
        this.refreshDebtorList.emit();
      },
        error => {
          this.matSnackBar.open(error.error.code, "X", {
            duration: 2000,
          });
        });
    }
  }
}


