import { NgModule } from '@angular/core';
import { NavComponent } from './nav.component';
import { SharedModule } from 'src/shared/shared.module';


@NgModule({
  declarations: [NavComponent],
  imports: [
    SharedModule
  ],
  exports: [
    NavComponent
  ]
})
export class NavModule { }
