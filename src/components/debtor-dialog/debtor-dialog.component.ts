import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Debtor } from '../../models/Debtor';
import { MapDialogComponent } from '../map-dialog/map-dialog.component';
import { DebtorsService } from 'src/services/debtors.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-debtor-dialog',
  templateUrl: './debtor-dialog.component.html',
  styleUrls: ['./debtor-dialog.component.css']
})
export class DebtorDialogComponent implements OnInit {
  debtorForm: FormGroup;

  constructor(private debtorsService: DebtorsService,
    public dialogRef: MatDialogRef<DebtorDialogComponent>,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: Debtor,
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar) { }

  ngOnInit() {
    this.debtorForm = this.formBuilder.group({
      name: [this.data.name, Validators.required],
      lastname: [this.data.lastname, Validators.required],
      age: [this.data.age, Validators.required],
      debt: [this.data.debt, Validators.required],
      location: [this.data.location, Validators.required]
    });
  }

  openDialog(): void {
    const dialogRef2 = this.dialog.open(MapDialogComponent, {
      width: '600px',
      data: {
        location: this.data.location,
        markerType: "debtor"
      }
    });

    dialogRef2.afterClosed().subscribe(result => {
      if (result) {
        this.data.location = result.location;
      }
    });
  }

  onSubmit() {
    this.debtorForm.markAllAsTouched();
    if (this.debtorForm.invalid) {
      return;
    }
    if (this.data.id) {
      this.updateDebtor();
    }
    else {
      this.addDebtor();
    }
  }

  addDebtor() {
    this.debtorsService.add(this.data).subscribe(result => {
      this.dialogRef.close(this.data);
    },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
  }
  updateDebtor() {
    this.debtorsService.update(this.data).subscribe(result => {
      this.dialogRef.close(this.data);
    },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
