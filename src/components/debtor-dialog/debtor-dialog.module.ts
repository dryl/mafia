import { NgModule } from '@angular/core';
import { SharedModule } from 'src/shared/shared.module';
import { DebtorDialogComponent } from './debtor-dialog.component';
import { FormsModule } from '@angular/forms';
import { MapDialogComponent } from '../map-dialog/map-dialog.component';
import { MapDialogModule } from '../map-dialog/map-dialog.module';
import { MapModule } from '../map/map.module';



@NgModule({
  declarations: [DebtorDialogComponent],
  imports: [
    SharedModule,
    FormsModule,
    MapDialogModule,
    MapModule
  ],
  entryComponents: [
    MapDialogComponent
  ]
})
export class DebtorDialogModule { }
