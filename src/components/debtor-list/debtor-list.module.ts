import { NgModule } from '@angular/core';
import { DebtorListComponent } from './debtor-list.component';
import { SharedModule } from 'src/shared/shared.module';
import { DebtorDialogComponent } from '../debtor-dialog/debtor-dialog.component';
import { DebtorDialogModule } from '../debtor-dialog/debtor-dialog.module';



@NgModule({
  declarations: [DebtorListComponent],
  imports: [
    SharedModule,
    DebtorDialogModule
  ],
  exports: [DebtorListComponent],
  entryComponents: [
    DebtorDialogComponent
  ]
})
export class DebtorListModule { }
