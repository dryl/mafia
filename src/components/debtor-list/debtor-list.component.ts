import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { DebtorsService } from 'src/services/debtors.service';
import { Debtor } from 'src/models/Debtor';
import { DebtorDialogComponent } from '../debtor-dialog/debtor-dialog.component';
import { MatDialog, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-debtor-list',
  templateUrl: './debtor-list.component.html',
  styleUrls: ['./debtor-list.component.css']
})
export class DebtorListComponent implements OnInit {

  debtorList: Debtor[];
  displayedColumns: string[];

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  dataSource: MatTableDataSource<Debtor>;
  
  @Output() findOnMap = new EventEmitter<string>();
  @Output() refreshKillerList = new EventEmitter<void>();
  @Output() refreshLocations = new EventEmitter<void>();
  @Input() mapActive: boolean;
  @Input() debtorWidgetActive: boolean;
  @Input() killerWidgetActive: boolean;

  constructor(private debtorsService: DebtorsService, public matDialog: MatDialog, private matSnackBar: MatSnackBar) { }

  ngOnInit() {
    this.displayedColumns = ['name', 'lastname', 'age', 'debt', 'location', 'action'];
    this.loadDebtors();
  }

  initSort() {
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'name': return item[property].toLowerCase();
        case 'lastname': return item[property].toLowerCase();
        case 'location': return item[property].toLowerCase();
        default: return item[property];
      }
    };
  }

  loadDebtors() {
    this.debtorsService.getList().subscribe(result => {
      this.debtorList = result;
      this.refreshLocations.emit();
      this.dataSource = new MatTableDataSource(this.debtorList);
      this.initSort();
    },
    error => {
      this.matSnackBar.open(error.error.code, "X", {
        duration: 2000,
      });
    });
  }

  findOnMapClick(location: string) {
    this.findOnMap.emit(location);
  }

  openDebtorDialog(debtorId: number) {
    var element = document.getElementById("map");
    var node = document.getElementById("map").parentNode;
    node.removeChild(element);

    if (debtorId) {
      var debtor: Debtor = this.debtorList.find(item => item.id == debtorId);
    }
    else {
      var debtor = new Debtor();
    }
    const dialog = this.matDialog.open(DebtorDialogComponent, {
      width: '600px',
      data: {
        id: debtor.id,
        name: debtor.name,
        lastname: debtor.lastname,
        age: debtor.age,
        debt: debtor.debt,
        isTarget: debtor.isTarget,
        location: debtor.location
      }
    });

    dialog.afterClosed().subscribe(result => {
      node.appendChild(element);
      this.loadDebtors();
    });
  }

  deleteDebtor(debtor: Debtor) {
    if (confirm("Czy na pewno chcesz usunąć wybranego dłużnika (" + debtor.name + " " + debtor.lastname + ")?")) {
      this.debtorsService.delete(debtor.id).subscribe(result => { 
        this.loadDebtors();
        this.refreshKillerList.emit();
      },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
    }
  }
  forgive(debtor: Debtor) {
    if (confirm("Czy na pewno chcesz wybaczyć wybranemu dłużnikowi (" + debtor.name + " " + debtor.lastname + ")?")) {
      this.debtorsService.cancelTask(debtor.id).subscribe(result => {
        if (result == "Deleted") {
          this.loadDebtors()
          this.refreshKillerList.emit();
        }
      },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
    }
  }
}
