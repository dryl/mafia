import { NgModule } from '@angular/core';
import { SharedModule } from 'src/shared/shared.module';
import { AssignmentDialogComponent } from './assignment-dialog.component';



@NgModule({
  declarations: [AssignmentDialogComponent],
  imports: [
    SharedModule
  ]
})
export class AssignmentDialogModule { }
