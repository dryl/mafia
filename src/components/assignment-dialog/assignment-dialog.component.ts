import { Component, OnInit, Inject } from '@angular/core';
import { Debtor } from 'src/models/Debtor';
import { DebtorsService } from 'src/services/debtors.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Killer } from 'src/models/Killer';
import { FormControl } from '@angular/forms';
import { KillersService } from 'src/services/killers.service';

@Component({
  selector: 'app-assignment-dialog',
  templateUrl: './assignment-dialog.component.html',
  styleUrls: ['./assignment-dialog.component.css']
})
export class AssignmentDialogComponent implements OnInit {

  debtorList: Debtor[];
  isDisabled: boolean;
  targetControl = new FormControl('');

  constructor(private killersService: KillersService, 
    private debtorsService: DebtorsService, 
    public dialogRef: MatDialogRef<AssignmentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Killer,
    private matSnackBar: MatSnackBar) { }

  ngOnInit() {
    this.loadDebtors();
    this.isDisabled = this.data.targetId ? true : false;
  }

  loadDebtors() {
    this.debtorsService.getList().subscribe(result => this.debtorList = result);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  cancelTarget() {
    this.killersService.cancelTarget(this.data.id).subscribe(result => {
      if (result == "Deleted") {
        this.data.targetId = null;
        this.isDisabled = false;
      }
    },
    error => {
      this.matSnackBar.open(error.error.code, "X", {
        duration: 2000,
      });
    });
  }

  setTarget() {
    if (!this.isDisabled && this.data.targetId)
    {
      this.killersService.setTarget(this.data.id, this.data.targetId).subscribe(result => {
      },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
    }
  }
}
