import { Component, OnInit, Input, EventEmitter, Output, SimpleChanges } from '@angular/core';
import Map from 'ol/Map';
import Tile from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import View from 'ol/View';
import { fromLonLat, transform } from 'ol/proj';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import { Location } from '../../models/Location';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  map: any;
  coordinates: string;
  markerLayer: VectorLayer;

  @Input() debtorsLocation: Location[];
  @Input() killersLocation: Location[];

  @Input() location: string;
  @Input() markerType: string;
  @Output() locationChange = new EventEmitter<string>();
  @Input() addMarkerOnClick: boolean = false;

  constructor() { }

  ngOnInit() {
    this.loadMap();
    //   this.addKillerMarker(37.41, 8.82);
    if (this.map) {
      this.map.updateSize();
    }
    if (this.location) {
      let location = Location.parseLocation(this.location);
      if (this.markerType == "debtor") {
        this.markerLayer = this.addDebtorMarker(location.latitude, location.longitude);
      }
      if (this.markerType == "killer") {
        this.markerLayer = this.addKillerMarker(location.latitude, location.longitude);
      }
      this.zoomToPosition(location.latitude, location.longitude);
    }
    let that = this;
    setTimeout(function () {
      that.updateMap();
    }, 100);
  }
  updateMap() {
    if (this.map) {
      this.map.updateSize();
      if (this.debtorsLocation != null && this.killersLocation != null) {
        this.addDebtorsMarkers();
        this.addKillersMarkers();
      }
      if (this.location) {
        let location = Location.parseLocation(this.location);
        if (this.markerType == "debtor") {
          this.markerLayer = this.addDebtorMarker(location.latitude, location.longitude);
        }
        if (this.markerType == "killer") {
          this.markerLayer = this.addKillerMarker(location.latitude, location.longitude);
        }
        this.zoomToPosition(location.latitude, location.longitude);
      }
    }
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges["killersLocation"]) {
      this.killersLocation = simpleChanges["killersLocation"].currentValue;
    }
    if (simpleChanges["debtorsLocation"]) {
      this.debtorsLocation = simpleChanges["debtorsLocation"].currentValue;
    }
    if (this.map && this.debtorsLocation != null && this.killersLocation != null) {
      this.map.updateSize();
      this.addDebtorsMarkers();
      this.addKillersMarkers();
    }
  }

  loadMap() {
    this.map = new Map({
      target: 'map',
      layers: [
        new Tile({
          source: new OSM()
        })
      ],
      view: new View({
        center: fromLonLat([37.41, 8.82]),
        zoom: 2
      }),
      pixelRatio: 1
    })
  }

  addDebtorMarker(latitude, longitude) {
    var vectorLayer = new VectorLayer({
      source: new VectorSource({
        features: [new Feature({
          geometry: new Point(fromLonLat([latitude, longitude])),
        })]
      }),
      style: new Style({
        image: new Icon({
          anchor: [0.5, 0.5],
          src: "assets/images/DebtorMarker.png"
        })
      })
    });
    this.map.addLayer(vectorLayer);
    return vectorLayer;
  }

  addKillerMarker(latitude, longitude) {
    var vectorLayer = new VectorLayer({
      source: new VectorSource({
        features: [new Feature({
          geometry: new Point(fromLonLat([latitude, longitude])),
        })]
      }),
      style: new Style({
        image: new Icon({
          anchor: [0.5, 0.5],
          src: "assets/images/KillerMarker.png"
        })
      })
    });
    this.map.addLayer(vectorLayer);
    return vectorLayer;
  }

  addDebtorsMarkers() {
    this.debtorsLocation.forEach(item => {
      this.addDebtorMarker(item.latitude, item.longitude);
    });
  }

  addKillersMarkers() {
    this.killersLocation.forEach(item => {
      this.addKillerMarker(item.latitude, item.longitude);
    });
  }

  zoomToPosition(latitude, longitude) {
    this.map.getView().setCenter(transform([latitude, longitude], 'EPSG:4326', 'EPSG:3857'));
    this.map.getView().setZoom(10);
  }

  onMouseMove(event) {
    this.coordinates = ""
    var lonlat = transform(this.map.getEventCoordinate(event), 'EPSG:3857', 'EPSG:4326');
    this.coordinates += lonlat[0].toFixed(2);
    this.coordinates += "/";
    this.coordinates += lonlat[1].toFixed(2);
  }

  onMouseDown(event) {
    if (this.addMarkerOnClick) {
      let location = new Location();
      var lonlat = transform(this.map.getEventCoordinate(event), 'EPSG:3857', 'EPSG:4326');
      if (this.markerLayer) {
        this.map.removeLayer(this.markerLayer);
      }
      if (this.markerType == "debtor") {
        this.markerLayer = this.addDebtorMarker(lonlat[0], lonlat[1]);
      }
      if (this.markerType == "killer") {
        this.markerLayer = this.addKillerMarker(lonlat[0], lonlat[1]);
      }
      location.latitude = lonlat[0];
      location.longitude = lonlat[1];
      this.location = location.toString();
      this.locationChange.emit(this.location);
    }
  }
}
