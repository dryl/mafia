import { NgModule } from '@angular/core';
import { LoginFormComponent } from './login-form.component';
import { SharedModule } from 'src/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [LoginFormComponent],
  imports: [
  SharedModule,
  ReactiveFormsModule
  ]
})
export class LoginFormModule { }
