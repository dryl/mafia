import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Killer } from 'src/models/Killer';
import { MapDialogComponent } from '../map-dialog/map-dialog.component';
import { KillersService } from 'src/services/killers.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-killer-dialog',
  templateUrl: './killer-dialog.component.html',
  styleUrls: ['./killer-dialog.component.css']
})
export class KillerDialogComponent implements OnInit {
  killerForm: FormGroup;

  constructor(private killersService: KillersService,
    public dialogRef: MatDialogRef<KillerDialogComponent>,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: Killer,
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar) { }

  ngOnInit() {
    this.killerForm = this.formBuilder.group({
      pseudonym: [this.data.pseudonym, Validators.required],
      salary: [this.data.salary, Validators.required],
      location: [this.data.location, Validators.required],
    });
  }

  openDialog(): void {
    const dialogRef2 = this.dialog.open(MapDialogComponent, {
      width: '600px',
      data: {
        location: this.data.location,
        markerType: "killer"
      }
    });

    dialogRef2.afterClosed().subscribe(result => {
      if (result) {
        this.data.location = result.location;
      }
    });
  }

  onSubmit() {
    this.killerForm.markAllAsTouched();
    if (this.killerForm.invalid) {
      return;
    }

    if (this.data.id) {
      this.updateKiller();
    }
    else {
      this.addKiller();
    }
  }

  addKiller() {
    this.killersService.add(this.data).subscribe(result => {
      this.dialogRef.close(this.data);
    },
      error => {
        this.matSnackBar.open(error.error.code, "X", {
          duration: 2000,
        });
      });
  }

  updateKiller() {
    this.killersService.update(this.data).subscribe(result => {
      this.dialogRef.close(this.data);
    },
    error => {
      this.matSnackBar.open(error.error.code, "X", {
        duration: 2000,
      });
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
