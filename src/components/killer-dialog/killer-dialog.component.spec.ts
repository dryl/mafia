import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KillerDialogComponent } from './killer-dialog.component';

describe('KillerDialogComponent', () => {
  let component: KillerDialogComponent;
  let fixture: ComponentFixture<KillerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KillerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KillerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
