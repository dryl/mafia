import { NgModule } from '@angular/core';
import { SharedModule } from 'src/shared/shared.module';
import { KillerDialogComponent } from './killer-dialog.component';
import { FormsModule } from '@angular/forms';
import { MapDialogComponent } from '../map-dialog/map-dialog.component';
import { MapDialogModule } from '../map-dialog/map-dialog.module';
import { MapModule } from '../map/map.module';



@NgModule({
  declarations: [KillerDialogComponent],
  imports: [
    SharedModule,
    FormsModule,
    MapDialogModule,
    MapModule
  ],
  entryComponents: [
    MapDialogComponent
  ]
})
export class KillerDialogModule { }
