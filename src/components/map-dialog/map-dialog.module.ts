import { NgModule } from '@angular/core';
import { MapDialogComponent } from './map-dialog.component';
import { SharedModule } from 'src/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MapModule } from '../map/map.module';



@NgModule({
  declarations: [MapDialogComponent],
  imports: [
    SharedModule,
    FormsModule,
    MapModule
  ]
})
export class MapDialogModule { }
