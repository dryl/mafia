import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from 'src/components/login-form/login-form.component';
import { DashboardComponent } from 'src/components/dashboard/dashboard.component';
import { AuthGuardService } from 'src/services/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: '*', redirectTo: '' },
  {path: 'login', component: LoginFormComponent },
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
