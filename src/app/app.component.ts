
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Mafia';
  user: User;

  constructor(
    private router: Router,
    private authService: AuthService
) {
    this.authService.user.subscribe(x => this.user = x);
}

logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
}
}