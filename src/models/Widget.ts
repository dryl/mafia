import { Rectangle } from 'ngx-widget-grid';

class Position {
top: number;
left: number;
width: number;
height: number;
}
class Options {
    title: string;
}
export class Widget {
   position: Rectangle;
   type: number;
   options: Options;
}