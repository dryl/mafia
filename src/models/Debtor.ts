export class Debtor {
    id: number;
    name: string;
    lastname: string;
    location: string;
    debt: number;
    age: number;
    isTarget: boolean;
}