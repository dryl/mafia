import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import {ApiConfig} from '../ApiConfig';

@Injectable({ providedIn: 'root' })
export class AuthService {

    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

    constructor(private http: HttpClient) {
        this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('User')));
        this.user = this.userSubject.asObservable();
    }

    public get userValue(): User {
        return this.userSubject.value;
    }

    login(login: string, password: string) {
        return this.http.post<any>(`${ApiConfig.apiUrl}/user/login`, { login, password })
            .pipe(map(user => {
                localStorage.setItem('User', JSON.stringify(user));
                this.userSubject.next(user);
                return user;
            }));
    }
    
    logout() {
        localStorage.removeItem('User');
        this.userSubject.next(null);
    }
}