import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfig } from '../ApiConfig';
import { Killer } from 'src/models/Killer';

@Injectable({ providedIn: 'root' })
export class KillersService {
    constructor(private http: HttpClient) { }

    getList() {
        return this.http.get<Killer[]>(`${ApiConfig.apiUrl}/killers/list`);
    }

    add(killer: Killer) {
        return this.http.post<any>(`${ApiConfig.apiUrl}/killers/add`, killer);
    }
    update(killer: Killer) {
        return this.http.put<any>(`${ApiConfig.apiUrl}/killers/edit/` + killer.id, killer);
    }
    delete(killerId: number) {
        return this.http.delete<any>(`${ApiConfig.apiUrl}/killers/remove/` + killerId);
    }
    setTarget(killerId: number, debtorId: number) {
        return this.http.post<any>(`${ApiConfig.apiUrl}/killers/set-target/`, {"killerId": killerId, "targetId": debtorId});
    }
    cancelTarget(killerId: number) {
        return this.http.delete<any>(`${ApiConfig.apiUrl}/killers/cancel-target/` + killerId);
    }
}

