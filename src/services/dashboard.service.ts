import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfig } from '../ApiConfig';
import { Widget } from 'src/models/Widget';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient, public authService: AuthService) { }

  getDashboard() {
    return this.http.get<any>(`${ApiConfig.apiUrl}/dashboard/get/` + "1");
  }
  save(widgets: Widget[]) {
    console.log(JSON.stringify(widgets));
    return this.http.post<any>(`${ApiConfig.apiUrl}/dashboard/add`, {userId: this.authService.userValue.id, widgets: JSON.stringify(widgets)});
  }
  getDefaultDashboard() {
    return this.http.get<any>("./assets/defaultDashboard.json");
  }
}

