import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfig } from '../ApiConfig';
import { Debtor } from '../models/Debtor';

@Injectable({ providedIn: 'root' })
export class DebtorsService {
    constructor(private http: HttpClient) { }

    getList() {
        return this.http.get<Debtor[]>(`${ApiConfig.apiUrl}/debtors/list`);
    }
    add(debtor: Debtor) {
        return this.http.post<any>(`${ApiConfig.apiUrl}/debtors/add`, debtor);
    }
    update(debtor: Debtor) {
        return this.http.put<any>(`${ApiConfig.apiUrl}/debtors/edit/` + debtor.id, debtor);
    }
    delete(debtorId: number) {
        return this.http.delete<any>(`${ApiConfig.apiUrl}/debtors/remove/` + debtorId);
    }
    cancelTask(debtorId: number) {
        return this.http.delete<any>(`${ApiConfig.apiUrl}/debtors/cancel-task/` + debtorId);
    }
}


