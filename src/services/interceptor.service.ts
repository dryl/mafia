import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class InterceptorService implements HttpInterceptor {
    constructor(private authService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let user = this.authService.userValue;
        if (user && user.userSession) {
            request = request.clone({
                setHeaders: {
                    'user-session': `${user.userSession}`
                }
            });
        }
        return next.handle(request);
    }
}